﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECommerce2.Models
{
    public class Category
    {
        [Key]

        public int CategoryId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe tener maximo {1} caracteres")]        
        [Index("Category_CategoryId_Description_Index", 2, IsUnique = true)]
        [Display(Name = "Category")]

        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Index("Category_CategoryId_Description_Index", 1, IsUnique = true)]
        [Display(Name = "Company")]
        
        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}