﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ECommerce2.Models
{
    public class Tax
    {
        [Key]

        public int TaxId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe tener maximo {1} caracteres")]
        [Index("Tax_CategoryId_Description_Index", 2, IsUnique = true)]
        [Display(Name = "Tax")]

        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:P2}", ApplyFormatInEditMode = false)]
        [Range(0,1 , ErrorMessage = "debe seleccionar a {0} entre {1} and {2}")]
        public double Rate { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Index("Tax_CategoryId_Description_Index", 1, IsUnique = true)]
        [Display(Name = "Company")]

        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}