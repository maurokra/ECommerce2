﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ECommerce2.Models
{
    public class Product
    {
        [Key]

        public int ProductId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Index("Product_CategoryId_Description_Index", 1, IsUnique = true)]
        [Index("Product_CategoryId_BarCode_Index", 1, IsUnique = true)]
        [Display(Name = "Company")]

        public int CompanyId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe tener maximo {1} caracteres")]
        [Index("Product_CategoryId_Description_Index", 2, IsUnique = true)]
        [Display(Name = "Product")]

        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(13, ErrorMessage = "El campo {0} debe tener maximo {1} caracteres")]
        [Index("Product_CategoryId_BarCode_Index", 2, IsUnique = true)]
        [Display(Name = "Bar Code")]

        public string BarCode { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Display(Name = "Category")]

        public int CategoryId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Display(Name = "Tax")]
        public int TaxId { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(0, double.MaxValue, ErrorMessage = "debe seleccionar a {0} entre {1} and {2}")]

        public decimal Price { get; set; }

        [DataType(DataType.ImageUrl)]

        public string Image { get; set; }

        [NotMapped]
        [Display(Name = "Image")]
        public HttpPostedFileBase ImageFile { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]

        public double Stock { get { return Inventories.Sum(i => i.Stock); } }

        public virtual Company Company { get; set; }

        public virtual Category Category { get; set; }

        public virtual Tax Tax { get; set; }

        public virtual ICollection<Inventory> Inventories { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        public virtual ICollection<OrderDetailTmp> OrderDetailTmps { get; set; }
    }
}