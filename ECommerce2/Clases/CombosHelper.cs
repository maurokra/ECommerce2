﻿using ECommerce2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace ECommerce2.Clases
{
    public class CombosHelper : IDisposable
    {
        private static ECommerce2Context db = new ECommerce2Context();

        public static List<Department> GetDepartments()
        {
            var departments = db.Departments.ToList();
            departments.Add(new Department
            {
                DepartmentId = 0,
                Name = "[Seleccione un departamento...]",
            });
            return departments = departments.OrderBy(d => d.Name).ToList();
        }

        public static List<Product> GetProducts(int companyId, bool sw)
        {
            var products = db.Products.Where(p => p.CompanyId == companyId).ToList();            
            return products.OrderBy(p => p.Description).ToList();
        }

        public static List<Product> GetProducts(int companyId)
        {
            var products = db.Products.Where(p => p.CompanyId == companyId).ToList();
            products.Add(new Product
            {
                ProductId = 0,
                Description = "[Seleccione un product...]",
            });
            return products.OrderBy(p => p.Description).ToList();
        }

        public static List<City> GetCities(int departmentId)
        {
            var cities = db.Cities.Where(c => c.DepartmentId == departmentId).ToList();
            cities.Add(new City
            {
                CityId = 0,
                Name = "[Seleccione un city...]",
            });
            return cities.OrderBy(d => d.Name).ToList();            
        }

        public static List<Company> GetCompanies()
        {
            var Companies = db.Companies.ToList();
            Companies.Add(new Company
            {
                CompanyId = 0,
                Name = "[Seleccione un Company...]",
            });
            return Companies.OrderBy(d => d.Name).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public static List<Customer> GetCustomers(int companyId)
        {
            var qry = (from cu in db.Customers
                       join cc in db.CompanyCustomers on cu.CustomerId equals cc.CustomerId
                       join co in db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { cu }).ToList();
            var customers = new List<Customer>();
            foreach(var item in qry)
            {
                customers.Add(item.cu);
            }

            //var Customers = db.Customers.Where(c => c.CompanyId == companyId).ToList();
            customers.Add(new Customer
            {
                CustomerId = 0,
                FirstName = "[Seleccione un Customers...]",
            });
            return customers.OrderBy(c => c.FirstName).ThenBy(c => c.LastName).ToList();

        }

        public static List<Tax> GetTaxes(int companyId)
        {
            var taxes = db.Taxes.Where(c => c.CompanyId == companyId).ToList();
            taxes.Add(new Tax
            {
                TaxId = 0,
                Description = "[Seleccione un Tax...]",
            });
            return taxes.OrderBy(d => d.Description).ToList();
        }

        public static List<Category> GetCategories(int companyId)
        {
            var categories = db.Categories.Where(c => c.CompanyId == companyId).ToList();
            categories.Add(new Category
            {
                CategoryId = 0,
                Description = "[Seleccione un Category...]",
            });
            return categories.OrderBy(d => d.Description).ToList();
        }
    }
}